﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Line
    {
        protected string symbol;

        public string Symbol
        {
            get { return symbol; }
            set { symbol = value; }
        }
        protected int number;

        public int Number
        {
            get { return number; }
            set { number = value; }
        }

        public void Print()
        {
            string header = "**********" + GetType().Name + "**********" ;
            
            Console.WriteLine(header);
            Console.WriteLine();
            Draw();
            Console.WriteLine();
            Console.WriteLine(new String('*',header.Length));
            Console.WriteLine();
        }
        public virtual void Draw()
        {
            for (int i = 0; i < number; i++)
            {
                Console.Write(symbol+" ");
                
            }
            Console.WriteLine();
        }

    }
}
